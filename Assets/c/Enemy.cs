using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public float Hp=100;
    NavMeshAgent agent;
    public GameObject player;
    public GameObject emission;
    public GameObject bullet;
    float theta ;
    float r=10;
    float ran_x;
    float ran_y;
    float ran_CD=5;
    float shoot_CD;
    public Vector3 ran;
    public int State;
    void Start() 
    {
        agent=GetComponent<NavMeshAgent>();
        agent.isStopped = false;
        player=GameObject.Find("player");
        Hp=100;
        State=0;
        //State 0=Patrol 1=Chasing 2=Shooting 3=Escape
    }
    void Update() 
    {
        Vector3 playerPos = player.transform.position;
        Vector3 MyPos = transform.position;
        if(State==0) 
        {
            Patrol();
            if(Vector3.Distance(playerPos,MyPos)<15)
            {
                State=1;
            }
        }
        if(State==1)
        {
            Chasing();
            if(Vector3.Distance(playerPos,MyPos)>20)
            {
                State=0;
            }
            if(Vector3.Distance(playerPos,MyPos)<10)
            {
                State=2;
            }
        }
        if(State==2)
        {
            Shooting();
            if(Vector3.Distance(playerPos,MyPos)>13)
            {
                State=1;
            }
            if(Vector3.Distance(playerPos,MyPos)<5)
            {
                State=3;
            }
        }
        if(State==3)
        {
            Escape();
            if(Vector3.Distance(playerPos,MyPos)>10)
            {
                State=2;
            }
        }
         
    }
    void Patrol()
    {
        //巡邏
        ran_CD+=Time.deltaTime;
        agent.speed=3.5f;
        if(ran_CD>=5)
        {
            theta=Random.Range(0,Mathf.PI*2);
            ran_x=Mathf.Cos(theta)*r;
            ran_y=Mathf.Sin(theta)*r;
            ran_CD=0;
            ran=transform.position+(new Vector3(ran_x,0,ran_y));
        }
        agent.SetDestination(ran);

        Debug.Log(ran);
    }
    void Chasing()
    {
        //追逐
        agent.speed=3.5f;
        agent.SetDestination(player.transform.position);
    }
    void Shooting()
    {
        //攻擊
        shoot_CD+=Time.deltaTime;
        agent.speed=1f;
        agent.SetDestination(player.transform.position);
        if(shoot_CD>=0.5f)
        {
        Instantiate(bullet,emission.transform.position,emission.transform.rotation);
        shoot_CD=0;
        }
    }
    void Escape()
    {
        //逃離
        Vector3 dir = transform.position - player.transform.position;
        agent.speed=5f;
        agent.SetDestination(transform.position + dir * 3f);
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow; 
        GizmosHelper.DrawCircle( transform.position,  15f );

        Gizmos.color = Color.red; 
        GizmosHelper.DrawCircle( transform.position,  20f );
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="bullet")
        {
            Destroy(other.gameObject);
            Hp-=10;
            if(Hp<=0)
            {
                Destroy(gameObject,0.5f);
            }
        }
    }
}
