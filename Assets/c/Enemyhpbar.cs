using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemyhpbar : MonoBehaviour
{
    public Enemy TargetEnemy;
    public Image Bar;
    public GameObject canvas;

    private RectTransform rectTrans;

    void Start() {
        rectTrans = GetComponent<RectTransform>();
       imagetocanvas();  
    }

    void Update()
    {
        UpdatePosition();
        UpdateBarWidth();
    }

    // 更新血條座標
    void UpdatePosition() {
        Vector3 enemyScreenPos = Camera.main.WorldToScreenPoint(TargetEnemy.transform.position);
        Vector3 offset = new Vector3(0, -80, 0);
        transform.position = enemyScreenPos + offset;
    }

    // 更新血條長度
    void UpdateBarWidth() {
        float hpScale = TargetEnemy.Hp * 0.01f;
        Bar.rectTransform.localScale = new Vector3(
            hpScale, 
            Bar.rectTransform.localScale.y, 
            Bar.rectTransform.localScale.z);
            if(hpScale<=0)
            {
                Destroy(gameObject);
            }
    }
    void imagetocanvas()
    {
        //把image放到canvas上
        canvas=GameObject.Find("Canvas");
        transform.SetParent(canvas.transform);
    }
}
