using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float HP=100;
    Rigidbody rb;
    float speed=50f;
    public Joystick joystick;
    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }
    void move()
    {
        //角色移動&轉向
        float h=joystick.Horizontal;
        float v=joystick.Vertical;
        Vector3 m_input=new Vector3(h, 0, v);
        if(m_input.magnitude>0.1f)
        {
        float faceAngle = Mathf.Atan2(h, v) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, faceAngle, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.05f);
        }
        rb.MovePosition(transform.position+m_input*Time.deltaTime*speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="bullet")
        {
            Destroy(other.gameObject);
            HP-=10;
        }
    }
}
