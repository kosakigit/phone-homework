using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerHpbar : MonoBehaviour
{
    public Player TargetPlayer;
    public Image Bar;

    private RectTransform rectTrans;

    void Start() 
    {
        rectTrans = GetComponent<RectTransform>(); 
    }

    void Update()
    {
        UpdatePosition();
        UpdateBarWidth();
    }

    // 更新血條座標
    void UpdatePosition() 
    {
        Vector3 enemyScreenPos = Camera.main.WorldToScreenPoint(TargetPlayer.transform.position);
        Vector3 offset = new Vector3(0, -80, 0);
        transform.position = enemyScreenPos + offset;
    }

    // 更新血條長度
    void UpdateBarWidth() 
    {
        float hpScale = TargetPlayer.HP * 0.01f;
        Bar.rectTransform.localScale = new Vector3(
            hpScale, 
            Bar.rectTransform.localScale.y, 
            Bar.rectTransform.localScale.z);
            if(hpScale<=0)
            {
                Destroy(gameObject);
            }
    }
}
